-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 31, 2020 at 08:28 AM
-- Server version: 5.7.25-28-log
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbnt9wedaqpvj2`
--

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `ID` int(20) NOT NULL,
  `Type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`ID`, `Type`) VALUES
(1, 'Historical'),
(2, 'Modern');

-- --------------------------------------------------------

--
-- Table structure for table `Sight`
--

CREATE TABLE `Sight` (
  `ID` int(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(10000) NOT NULL,
  `URL` varchar(500) NOT NULL,
  `Latitude` varchar(50) NOT NULL,
  `Longitude` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Sight`
--

INSERT INTO `Sight` (`ID`, `Name`, `Description`, `URL`, `Latitude`, `Longitude`) VALUES
(1, 'Vanemuine ', 'Vanemuine (literal translation from Estonian: Eldermost) is a theatre in Tartu, (Estonia). It was the first Estonian language theatre.', 'https://upload.wikimedia.org/wikipedia/commons/a/a2/Vanemuine_theatre_%28Gardmanahay%29.JPG', '58.376496', '26.724041'),
(2, 'Kissing Students', 'The Kissing Students\' sculpture and fountain is one of the most recognised symbols of Tartu. A fountain has stood in the same place since 1948.', 'https://static2.visitestonia.com/images/3385572/The+Sculpture+of+Kissing+Students+%284%29.jpeg', '58.380123', '26.722251'),
(3, 'Estonian National Museum', 'The museum tracks the history, life and traditions of the Estonian people, presents the culture and history of other Finno-Ugric peoples, and the minorities in Estonia. It has a comprehensive display of traditional Estonian national costumes form all regions. A collection of wood carved beer tankards illustrates the traditional peasant fests and holidays. The exhibition includes an array of other handicrafts from hand-woven carpets to linen tablecloths.', 'https://www.erm.ee/sites/default/files/erm2016.png', '58.395294', '26.7420904');

-- --------------------------------------------------------

--
-- Table structure for table `Sight_Categories`
--

CREATE TABLE `Sight_Categories` (
  `Sight_CatID` int(20) NOT NULL,
  `SightID` int(20) NOT NULL,
  `CatID` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Sight_Categories`
--

INSERT INTO `Sight_Categories` (`Sight_CatID`, `SightID`, `CatID`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Sight`
--
ALTER TABLE `Sight`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Sight_Categories`
--
ALTER TABLE `Sight_Categories`
  ADD PRIMARY KEY (`Sight_CatID`),
  ADD KEY `SightID` (`SightID`),
  ADD KEY `Sight_Categories_ibfk_1` (`CatID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Sight`
--
ALTER TABLE `Sight`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Sight_Categories`
--
ALTER TABLE `Sight_Categories`
  MODIFY `Sight_CatID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Sight_Categories`
--
ALTER TABLE `Sight_Categories`
  ADD CONSTRAINT `Sight_Categories_ibfk_1` FOREIGN KEY (`CatID`) REFERENCES `Categories` (`ID`),
  ADD CONSTRAINT `Sight_Categories_ibfk_2` FOREIGN KEY (`SightID`) REFERENCES `Sight` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
