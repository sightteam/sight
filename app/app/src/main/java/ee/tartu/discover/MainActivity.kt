package ee.tartu.discover

import android.Manifest
import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

import ee.tartu.discover.api.endpoints.SightApi
import ee.tartu.discover.api.entities.SightEntity
import ee.tartu.discover.api.getRetrofit
import ee.tartu.discover.tools.FavoriteRepo
import ee.tartu.discover.tools.OnSwipeTouchListener
import ee.tartu.discover.tools.SettingsRepo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_main_bottom_sheet.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    var api = getRetrofit()
        .create(SightApi::class.java)
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    var settings: MutableList<String> = mutableListOf()

    var sightIdValue: Int = 0
    var sortedSightsList: MutableList<SightEntity> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        settings = SettingsRepo(this).loadSettings() as MutableList<String>

        super.onCreate(savedInstanceState)

        if (!SettingsRepo(this).isTutorialFinished()) {

            val intent = Intent(this, TutorialActivity::class.java)
            startActivity(intent)
            finish()

        }
        setContentView(R.layout.activity_main)

        sightSettings.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        locationAPICall()
    }

    override fun onResume() {
        settings = SettingsRepo(this).loadSettings() as MutableList<String>
        super.onResume()
        if (settingsChanged) {
            locationAPICall()
            settingsChanged = false
        }
    }

    fun loadSights(latitude: Double?, longitude: Double?) {

        val settings = SettingsRepo(this).loadSettings()
        val radius = SettingsRepo(this).loadRadius()

        progressSpinner.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val sights = api.getSight(latitude, longitude).await()

                sortedSightsList =
                    filterSights(
                        sights,
                        settings,
                        radius
                    )

                sightIdValue = 0

                CoroutineScope(Dispatchers.Main).launch {
                    progressSpinner.visibility = View.GONE

                    loadSight(sightIdValue)
                }
            } catch (e: Exception) {

                CoroutineScope(Dispatchers.Main).launch {
                    progressSpinner.visibility = View.GONE
                    noInternetDialog()
                }

            }
        }
    }

    fun loadSight(index: Int) {

        if (index >= sortedSightsList.size) {
            Toast.makeText(this, "Filtering settings too strict, reset settings", Toast.LENGTH_LONG)
                .show()
            return
        }

        val currentSight: SightEntity = sortedSightsList.get(index)
        //Sight Picture
        val imagePath = "https://kalm.ee/samolberg/discover/pictures/"
        Picasso.get().load(imagePath + currentSight.id + ".jpg").into(landingImage)
        //Sight Description
        sightDescription.setText(currentSight.description)

        sightName.setText(currentSight.name)

        //Sight Distance
        var currentSightDistance = currentSight.distance

        if (currentSightDistance > 2000) {
            var currentSightDistanceKilometers = currentSight.distance.toDouble().div(1000)
            sightDistance.setText(currentSightDistanceKilometers.toString().plus(" km"))
        } else {
            sightDistance.setText(currentSightDistance.toString().plus(" m"))
        }

        if (currentSightDistance == 0) {
            sightDistance.setText(this.resources.getString(R.string.sightDistanceNull))
        }

        //Google Maps Button
        sightMaps.setOnClickListener {

            currentSight.navigate(this)
        }
        var clickCount = 0
        val maxClick = 2
        val clickDelayTime: Long = 500
        val mCountDownTimer: CountDownTimer =
            object : CountDownTimer(clickDelayTime, clickDelayTime) {
                override fun onTick(millisUntilFinished: Long) {}
                override fun onFinish() {
                    clickCount = 0
                }
            }
        val context: Context = this
        activityMainLayout.setOnTouchListener(object : OnSwipeTouchListener(context) {

            override fun onSwipeLeft() {

                sightIdValue++
                if (sightIdValue >= sortedSightsList.size) {
                    sightIdValue = 0
                }
                loadSight(sightIdValue)
            }

            override fun onSwipeRight() {

                sightIdValue--
                if (sightIdValue < 0) {
                    sightIdValue = sortedSightsList.size - 1

                }
                loadSight(sightIdValue)

            }

            override fun onClick() {

                clickCount++

                mCountDownTimer.cancel()

                mCountDownTimer.start()

                if (clickCount == maxClick) {
                    clickCount = 0
                    currentSight.id?.let {
                        addSightToFavorite(it)
                    }

                }
            }

            override fun onSwipeTop() {
                var behavior = BottomSheetBehavior.from(sightDescriptionBottomSheet)

                behavior.setState(BottomSheetBehavior.STATE_DRAGGING)
            }

        })

        val favoriteList = FavoriteRepo(this).loadFavorite()
        if (favoriteList.contains(currentSight.id)) {
            favoriteStarFilled.visibility = View.VISIBLE
            favoriteStarEmpty.visibility = View.INVISIBLE
        } else {
            favoriteStarEmpty.visibility = View.VISIBLE
            favoriteStarFilled.visibility = View.INVISIBLE
        }


        //Go to Favorite List
        favoriteStarEmpty.setOnClickListener {
            val intent = Intent(this, FavoriteActivity::class.java)
            startActivity(intent)
        }
        favoriteStarFilled.setOnClickListener {
            val intent = Intent(this, FavoriteActivity::class.java)
            startActivity(intent)
        }
    }

    fun addSightToFavorite(id: Int) {
        FavoriteRepo(this).saveFavoriteItem(id)

        favoriteStarFilled.visibility = View.VISIBLE
        favoriteStarEmpty.visibility = View.INVISIBLE


        favoriteAnimation.visibility = View.VISIBLE
        favoriteAnimation.playAnimation()

        favoriteAnimation.addAnimatorListener(object :
            Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

                favoriteAnimation.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animator) {

                favoriteAnimation.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
            }
        })
    }

    fun noInternetDialog() {

        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.internetErrorTitle))
            .setMessage(resources.getString(R.string.internetErrorDescription))

            .setNegativeButton(resources.getString(R.string.internetErrorRefresh)) { _, _ ->
                locationAPICall()
            }
            .setPositiveButton(resources.getString(R.string.internetErrorExit)) { _, _ ->
                finish()
            }
            .setCancelable(false)
            .show()

    }

    fun locationAPICall() {

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    // Got last known location. In some rare situations this can be null.
                    loadSights(location?.latitude, location?.longitude)
                }
        } else {
            loadSights(null, null)
            sightMaps.visibility = View.INVISIBLE
            walkingIcon.visibility = View.GONE
            noGpsIcon.visibility = View.VISIBLE
        }
    }

    companion object {
        var settingsChanged = false

        fun filterSights(
            sights: List<SightEntity>,
            settings: List<String>,
            radius: Int
        ): MutableList<SightEntity> {

            var sortedSightsList: MutableList<SightEntity> = mutableListOf()


            if (settings.isEmpty()) {
                sortedSightsList = sights.toMutableList()
            }

            sights.forEach {
                val filterMatches = it.catList.any { settings.contains(it) }
                if (filterMatches) {
                    sortedSightsList.add(it)
                }
            }

            if (radius != 0) {
                sortedSightsList = sortedSightsList.filter {
                    val distance: Int = it.distance
                    //  Log.d("RADIUS", "$distance <= $radius")
                    distance <= radius.toDouble()
                }.toMutableList()
            }

            return sortedSightsList

        }

    }

}



