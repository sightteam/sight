package ee.tartu.discover.tools

import android.content.Context
import ee.tartu.discover.MainActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SettingsRepo(val context: Context) {
    val pref = context.getSharedPreferences("sights.app.settings", Context.MODE_PRIVATE)

    fun saveSettings(list: List<String>) {
        val editor = pref.edit()

        MainActivity.settingsChanged = true
        editor.putString("settings",
            listToString(
                list
            )
        )
        editor.apply()
    }

    fun loadSettings(): List<String> {

        return pref.getString("settings", "[]")?.let {
            stringToList(
                it
            )
        } ?: emptyList()
    }

    fun clearSettings() {
        val settings = loadSettings() as MutableList<String>
        settings.clear()
        saveRadius(0)
        saveSettings(emptyList())
    }

    fun saveRadius(progress: Int) {
        val editor = pref.edit()
        MainActivity.settingsChanged = true
        editor.putInt("seekBarRadius", progress.times(500))
        editor.apply()
    }

    fun loadRadius(): Int {
        return pref.getInt("seekBarRadius", 0)
    }

    fun isTutorialFinished(): Boolean {
        return pref.getBoolean("tutorialComplete", false)
    }

    fun setTutorialtoFinished() {
        val editor = pref.edit()
        MainActivity.settingsChanged = true
        editor.putBoolean("tutorialComplete", true)
        editor.apply()
    }


    companion object {
        fun listToString(list: List<String>): String {
            return Gson().toJson(list)
        }

        fun stringToList(string: String): List<String> {
            val type = object :
                TypeToken<List<String>>() {}.type

            return Gson().fromJson(string, type)
        }
    }
}