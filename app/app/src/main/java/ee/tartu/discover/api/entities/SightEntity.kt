package ee.tartu.discover.api.entities

import android.content.Context
import android.content.Intent
import android.net.Uri

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SightEntity {

    @Expose
    @SerializedName("ID")
    var id: Int? = null

    @Expose
    @SerializedName("CategoryID")
    var catId: Int? = null

    @Expose
    @SerializedName("Name")
    var name: String? = null

    @Expose
    @SerializedName("Description")
    var description: String? = null

    @Expose
    @SerializedName("URL")
    var url: String? = null

    @Expose
    @SerializedName("Latitude")
    var latitude: Double? = null

    @Expose
    @SerializedName("Longitude")
    var longitude: Double? = null

    @Expose
    @SerializedName("GoogleDurationMeters")
    var distance: Int = 0

    @Expose
    @SerializedName("Categories")
    var catList: List<String> = emptyList()

    fun navigate(context: Context){
        val gmmIntentUri =
            Uri.parse("google.navigation:q=$latitude,$longitude")

        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        if (mapIntent.resolveActivity(context.packageManager) != null) {
            context.startActivity(mapIntent)
        }
    }









}