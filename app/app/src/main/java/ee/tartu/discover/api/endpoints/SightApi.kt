package ee.tartu.discover.api.endpoints


import ee.tartu.discover.api.entities.SightEntity
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface SightApi {


    @GET("discover/api.php")
    fun getSight(@Query("latitude") latitude: Double?,
                 @Query("longitude") longitude: Double?):Deferred<List<SightEntity>>


}