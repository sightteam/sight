package ee.tartu.discover

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import ee.tartu.discover.api.endpoints.SightApi
import ee.tartu.discover.api.entities.SightEntity
import ee.tartu.discover.api.getRetrofit
import ee.tartu.discover.tools.FavoriteRecyclerAdapter
import ee.tartu.discover.tools.FavoriteRepo
import kotlinx.android.synthetic.main.activity_favorites.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FavoriteActivity : AppCompatActivity(), FavoriteRecyclerAdapter.OnItemClickListener {

    private lateinit var adapter: FavoriteRecyclerAdapter
    var api = getRetrofit()
        .create(SightApi::class.java)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)
        var favoriteList = FavoriteRepo(this).loadFavorite()


        if (favoriteList.isEmpty()) {
            emptyFavoriteList.visibility = View.VISIBLE
        }

        favoriteListView.layoutManager = LinearLayoutManager(this).apply {
            this.orientation = LinearLayoutManager.VERTICAL
        }
        adapter = FavoriteRecyclerAdapter(
            emptyList(),
            this
        )
        favoriteListView.adapter = adapter
        CoroutineScope(Dispatchers.IO).launch {

            val sights = api.getSight(null, null).await()
            val favoriteSights = sights.filter { favoriteList.contains(it.id) }

            CoroutineScope(Dispatchers.Main).launch {
                adapter.items = favoriteSights
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onNavigate(item: SightEntity) {
        item.navigate(this)
    }

    override fun onRemove(item: SightEntity) {
        item.id?.let {
            FavoriteRepo(this).removeFavorite(it)
            val list = adapter.items as MutableList<SightEntity>
            list.remove(item)
            adapter.items = list
            adapter.notifyDataSetChanged()

            if (list.isEmpty()) {
                emptyFavoriteList.visibility = View.VISIBLE
            }
        }
    }

}