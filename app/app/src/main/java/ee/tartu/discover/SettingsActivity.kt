package ee.tartu.discover

import android.content.Intent
import android.os.Bundle
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity

import ee.tartu.discover.tools.FavoriteRepo
import ee.tartu.discover.tools.SettingsRepo
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_settings.*


class SettingsActivity : AppCompatActivity() {
    var settings: MutableList<String> = mutableListOf()
    lateinit var settingsRepo: SettingsRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        settings = SettingsRepo(this).loadSettings() as MutableList<String>
        settingsRepo = SettingsRepo(this)
        settings = settingsRepo.loadSettings() as MutableList<String>
        setupChipCloud()

        var loadProgress = settingsRepo.loadRadius()

        if (loadProgress == 0) {
            settingsSeekBar.setProgress(20)
            settingsSeekBarValue.setText(getString(R.string.radiusSettingMax))
        } else {
            settingsSeekBar.setProgress(settingsRepo.loadRadius().div(500))
            settingsSeekBarValue.setText(
                SettingsRepo(
                    this
                ).loadRadius().toString().plus(" m"))
        }

        settingsClear.setOnClickListener {
            SettingsRepo(this).clearSettings()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        favoritesClear.setOnClickListener {
            FavoriteRepo(this).clearFavorite()

        }
        goToTutorial.setOnClickListener {
            val intentTutorial = Intent(this, TutorialActivity::class.java)
            startActivity(intentTutorial)
        }

        settingsSeekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onProgressChanged(
                seekBar: SeekBar, progress: Int,
                fromUser: Boolean
            ) {

                if (progress == 20) {
                    settingsSeekBarValue.setText(getString(R.string.radiusSettingMax))
                    settingsRepo.saveRadius(0)
                } else {
                    settingsSeekBarValue.setText(progress.times(500).toString().plus(" m"))
                    settingsRepo.saveRadius(progress)
                }
            }
        })

    }

    private fun setupChipCloud() {
        val settingsItems = arrayOf(
            "Historical",
            "Modern",
            "Science",
            "Museum",
            "Arts",
            "Park",
            "Shopping",
            "Statue",
            "Cafe"
        )
        for (item in settingsItems) {
            val chip = Chip(this)
            chip.text = item
            chip.isCheckable = true
            chip.isChecked = settings.contains(item)
            chip.setOnClickListener {
                MainActivity.settingsChanged = true
                if (chip.isChecked) {
                    settings.add(item)
                } else {
                    settings.remove(item)
                }
                settingsRepo.saveSettings(settings)
            }
            chips.addView(chip)
        }
    }
}
