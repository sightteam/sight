package ee.tartu.discover.tools


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import ee.tartu.discover.api.entities.SightEntity
import com.squareup.picasso.Picasso
import ee.tartu.discover.R
import kotlinx.android.synthetic.main.layout_favorite_item.view.*

class FavoriteRecyclerAdapter(
    var items: List<SightEntity>,
    private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<FavoriteRecyclerAdapter.FavoriteViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): FavoriteViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_favorite_item, viewGroup, false)
        return FavoriteViewHolder(
            v
        )
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {

        holder.bind(items[position])

        holder.favoriteGoogleMaps.setOnClickListener {
            listener.onNavigate(items[position])
        }
        holder.favoriteRemove.setOnClickListener {
            listener.onRemove(items[position])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class FavoriteViewHolder constructor(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        val favoriteName: TextView = itemView.favoriteName
        val favoriteImage: ImageView = itemView.favoriteImage
        val favoriteGoogleMaps: ImageView = itemView.favoriteGoogleMaps
        val favoriteRemove: ImageView = itemView.favoriteRemove

        fun bind(favoriteItem: SightEntity) {
            favoriteName.setText(favoriteItem.name)
            Picasso.get().load(favoriteItem.url).into(favoriteImage)
        }

    }

    interface OnItemClickListener {
        fun onNavigate(item: SightEntity)
        fun onRemove(item: SightEntity)
    }

}