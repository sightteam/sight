package ee.tartu.discover.tools

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class FavoriteRepo(val context: Context) {
    val pref = context.getSharedPreferences("sights.app.favorite", Context.MODE_PRIVATE)

    fun saveFavorite(list: List<Int>) {
        val editor = pref.edit()

        editor.putString("favorite",
            listToString(
                list
            )
        )
        editor.apply()
    }

    fun saveFavoriteItem(id: Int) {
        val favorite = loadFavorite() as MutableList<Int>
        if (!favorite.contains(id)) {
            favorite.add(id)
            saveFavorite(favorite)
        }
    }

    fun loadFavorite(): List<Int> {

        return pref.getString("favorite", "[]")?.let {
            stringToList(
                it
            )
        } ?: emptyList()
    }

    fun clearFavorite() {
        val favorite = loadFavorite() as MutableList<Int>
        favorite.clear()
        saveFavorite(emptyList())
    }

    fun removeFavorite(id: Int) {
        val favorite = loadFavorite() as MutableList<Int>
        favorite.remove(id)
        saveFavorite(favorite)
    }

    companion object {
        fun listToString(list: List<Int>): String {
            return Gson().toJson(list)
        }

        fun stringToList(string: String): List<Int> {
            val type = object :
                TypeToken<List<Int>>() {}.type

            return Gson().fromJson(string, type)
        }
    }
}