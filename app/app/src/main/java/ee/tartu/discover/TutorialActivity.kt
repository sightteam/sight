package ee.tartu.discover

import android.Manifest
import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import ee.tartu.discover.tools.OnSwipeTouchListener
import ee.tartu.discover.tools.SettingsRepo
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.layout_main_bottom_sheet_tutorial.*


class TutorialActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)


        var tutorialProgress = 0
        var behavior = BottomSheetBehavior.from(sightDescriptionBottomSheetTutorial)
        behavior.setState(BottomSheetBehavior.STATE_SETTLING)

        var clickCount = 0
        val maxClick = 2
        val clickDelayTime: Long = 500
        val mCountDownTimer: CountDownTimer =
            object : CountDownTimer(clickDelayTime, clickDelayTime) {
                override fun onTick(millisUntilFinished: Long) {}
                override fun onFinish() {
                    clickCount = 0
                }
            }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )

            ) {

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    1
                )
            }
        } else {
            // Permission has already been granted
        }


        tutorialCompleteAnimation.setOnClickListener {

            SettingsRepo(this).setTutorialtoFinished()

            val intentTutorial = Intent(this, MainActivity::class.java)
            startActivity(intentTutorial)
            finish()
        }

        Picasso.get()
            .load("https://i.insider.com/5dcb2c7e79d75703c73d0803?width=1100&format=jpeg&auto=webp")
            .into(tutorialImage)

        tutorialText.setText(getString(R.string.tutorialText1))

        val context: Context = this
        activityTutorialLayout.setOnTouchListener(object : OnSwipeTouchListener(context) {


            override fun onSwipeLeft() {

                tutorialAnimationLeft.visibility = View.GONE
                tutorialAnimationRight.visibility = View.VISIBLE
                Picasso.get()
                    .load("https://upload.wikimedia.org/wikipedia/commons/c/c7/Empire_State_Building_from_the_Top_of_the_Rock.jpg")
                    .into(tutorialImage)
                tutorialText.setText(getString(R.string.tutorialText2))
                tutorialProgress = 1

            }

            override fun onSwipeRight() {

                if (tutorialProgress == 1) {
                    tutorialAnimationRight.visibility = View.GONE
                    tutorialAnimationDoubleTap.visibility = View.VISIBLE
                    tutorialText.visibility = View.GONE

                    tutorialBottomSheetTutorial.visibility = View.VISIBLE
                    tutorialBottomSheetTutorial.setText(getString(R.string.tutorialText3))
                    tutorialProgress = 2
                }
            }

            override fun onClick() {

                clickCount++

                mCountDownTimer.cancel()

                mCountDownTimer.start()

                if (clickCount == maxClick && tutorialProgress == 2) {
                    clickCount = 0

                    tutorialText.setText(getString(R.string.tutorialText4))
                    favoriteAnimationTutorial.visibility = View.VISIBLE
                    favoriteAnimationTutorial.playAnimation()

                    favoriteAnimationTutorial.addAnimatorListener(object :
                        Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {
                            favoriteAnimationTutorial.visibility = View.VISIBLE
                        }

                        override fun onAnimationEnd(animation: Animator) {
                            favoriteAnimationTutorial.visibility = View.GONE
                        }

                        override fun onAnimationCancel(animation: Animator) {
                        }

                        override fun onAnimationRepeat(animation: Animator) {
                        }
                    })

                    tutorialAnimationUp.visibility = View.VISIBLE
                    tutorialText.visibility = View.GONE
                    tutorialBottomSheetTutorial.setText(getString(R.string.tutorialText4))
                    tutorialAnimationDoubleTap.visibility = View.GONE

                    tutorialProgress = 3
                }

            }
        })


        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (tutorialProgress == 3) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        tutorialAnimationUp.visibility = View.GONE
                        tutorialAnimationDown.visibility = View.VISIBLE
                        tutorialText.visibility = View.GONE

                        tutorialBottomSheetTutorial.setText(getString(R.string.tutorialText5))
                        tutorialProgress = 4
                    }
                }
                if (tutorialProgress == 4) {
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        tutorialAnimationDown.visibility = View.GONE
                        tutorialCompleteAnimation.visibility = View.VISIBLE

                        tutorialBottomSheetTutorial.visibility = View.GONE
                        tutorialText.visibility = View.VISIBLE

                        tutorialText.setText(getString(R.string.tutorialText6))

                    }
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
            }
        })

    }


}
