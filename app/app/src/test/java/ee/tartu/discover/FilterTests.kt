package ee.tartu.discover

import ee.tartu.discover.api.entities.SightEntity
import org.junit.Test

class FilterTests {

    @Test

    fun filterRadiusTest() {

        val sight1 = SightEntity()
        sight1.distance = 500
        val sight2 = SightEntity()
        sight2.distance = 1000

        val sights: List<SightEntity> = listOf(sight1, sight2)
        val settings = emptyList<String>()
        val radius = 700

        val sortedSights =
            MainActivity.filterSights(
                sights,
                settings,
                radius
            )

        assert(sortedSights[0].distance < 700)
        assert(sortedSights.size == 1)



    }
    @Test
    fun filterSettingsTest(){
        val sight1 = SightEntity()
        sight1.catList = listOf("Historic")
        val sight2 = SightEntity()
        sight2.catList = listOf("Shopping","Modern")

        val sights: List<SightEntity> = listOf(sight1, sight2)
        val settings = listOf("Modern")
        val radius = 0

        val sortedSights =
            MainActivity.filterSights(
                sights,
                settings,
                radius
            )

        assert(sortedSights[0].catList.contains("Modern") )
        assert(sortedSights.size == 1)
    }


}

/*
kasuta
testi käsitsi korra
kas töötab
2. tee sinna testi
val sight1 = SightEntity(data, data, radius= 500)
val sight2 = SightEntity(data, data, radius= 1000)
3.
list Sights = sight1, sight2
4 MainActivity.filter(list, null, 700)
5. kontrolli et uues listis oleks 1 mis on raadiusega 500
kas sa üldpildist saad aru?
ilma koodita?
 */