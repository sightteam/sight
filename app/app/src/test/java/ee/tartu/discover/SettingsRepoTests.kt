package ee.tartu.discover

import ee.tartu.discover.tools.SettingsRepo
import org.junit.Test

import org.junit.Assert.*

class SettingsRepoTests {
    @Test
    fun listToStringTest() {
        val emptyList: MutableList<String> = mutableListOf()
       val string = SettingsRepo.listToString(emptyList)
        assertEquals("[]", string)
    }
    @Test
    fun stringToListTest() {
        val string = "[]"
        val list = SettingsRepo.stringToList(string)
        assert(list.isEmpty())

    }
    @Test
    fun itemToListTest() {
        val string = "[\"Item\"]"
        val list = SettingsRepo.stringToList(string)
        assert(list.size==1)
        assert(list.get(0)=="Item")

    }
}

/*
Store empty list, confirm that you receive empty list

Store single item list, confirm result

Store multiple items, confirm result

Clear storage, confirm result
 */
